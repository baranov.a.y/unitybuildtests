﻿//Copyright: Made by Appfox

#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Build
{
    public class CustomBuilder
    {
        /// <summary>
        /// Дополнительный путь для билдов.
        /// </summary>
        public const string BUILD_PATH = "Builds\\";
        public const string iOSBuildPath = BUILD_PATH + "iOS\\";
        public const string WindowsBuildPath = BUILD_PATH + "Windows\\";
        public const string AndroidBuildPath = BUILD_PATH + "Android\\";

        /// <summary>
        /// Повысить версию.
        /// </summary>
        public static void IncreaseVersion()
        {
            string version = PlayerSettings.bundleVersion.Trim().Replace(",", ".");
            string[] numbers = version.Split('.');

            string newVesion = "";
            if (numbers.Length > 0)
            {
                string lastNumber = numbers[numbers.Length - 1];
                if (int.TryParse(lastNumber, out int result))
                {
                    ++result;

                    for (int i = 0; i < numbers.Length - 1; i++)
                    {
                        newVesion += numbers[i] + ".";
                    }

                    newVesion += result.ToString();
                }
                else
                {
                    UnityEngine.Debug.LogError("Version number invalid!");
                }
            }
            else
            {
                newVesion = "0.1";
            }

            PlayerSettings.bundleVersion = newVesion;
        }

        /// <summary>
        /// Получить имя и версию приложения из настроек.
        /// </summary>
        public static string nameAndVersion
        {
            get
            {
                string result = PlayerSettings.productName;
                result += "_V" + PlayerSettings.bundleVersion;

                return result;
            }
        }

        /// <summary>
        /// Получить список сцен для билда.
        /// </summary>
        /// <returns></returns>
        public static string[] GetScenes()
        {
            var projectScenes = EditorBuildSettings.scenes;
            List<string> scenesToBuild = new List<string>();
            for (int i = 0; i < projectScenes.Length; i++)
            {
                if (projectScenes[i].enabled)
                {
                    scenesToBuild.Add(projectScenes[i].path);
                }
            }

            return scenesToBuild.ToArray();
        }

        #region Build targets

        /// <summary>
        /// Исходная группа для билда до его сборки.
        /// </summary>
        public static BuildTargetGroup originalBuildTargetGroup = BuildTargetGroup.Standalone;

        /// <summary>
        /// Исходная целевая платформа для билда до его сборки.
        /// </summary>
        public static BuildTarget originalBuildTarget = BuildTarget.StandaloneWindows;

        /// <summary>
        /// Запомнить целевую платформу для билда до его сборки.
        /// </summary>
        public static void RememberBuildTarget()
        {
            originalBuildTarget = EditorUserBuildSettings.activeBuildTarget;
            originalBuildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
        }

        /// <summary>
        /// Восстановить целевую платформу для билда до его сборки.
        /// </summary>
        public static void GetBackOnOriginalTarget()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(originalBuildTargetGroup, originalBuildTarget);
        }

        #endregion Build targets


        /// <summary>
        /// Создать билд по заданным настройкам.
        /// </summary>
        /// <param name="buildPlayerOptions"></param>
        public static void CreateBiuld(BuildPlayerOptions buildPlayerOptions)
        {
            string[] scenes = GetScenes();

            buildPlayerOptions.scenes = scenes;
            buildPlayerOptions.options = BuildOptions.CompressWithLz4HC;

            BuildPipeline.BuildPlayer(buildPlayerOptions);

            UnityEngine.Debug.Log("Build was created!");
        }

        /// <summary>
        /// Создать билд по заданным настройкам с возвращением после 
        /// сборки к исходной целевой платформе.
        /// </summary>
        /// <param name="buildPlayerOptions"></param>
        public static void CreateBiuldWithBackToTarget(BuildPlayerOptions buildPlayerOptions)
        {
            CreateBiuld(buildPlayerOptions);
            GetBackOnOriginalTarget();
        }

        [MenuItem("Build/Build Android")]
        public static void BuildAndroid()
        {
            RememberBuildTarget();
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);

            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            IncreaseVersion();
            buildPlayerOptions.locationPathName = AndroidBuildPath + nameAndVersion + ".apk";
            buildPlayerOptions.target = BuildTarget.Android;

            CreateBiuldWithBackToTarget(buildPlayerOptions);
        }
    }
}

#endif