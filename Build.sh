#!/bin/bash

LOG_FILE="build.log"

# Путь к утилите Unity CLI
UNITY_CLI="E:\Program Files\Unity\Editor\2019.4.1f1\Editor\Unity.exe"

# Путь до проекта
PROJECT_PATH="$PWD"

# Метод для вызова  
METHOD_NAME="Build.CustomBuilder.BuildAndroid"

# Запуск CLI
"$UNITY_CLI" -projectPath "$PROJECT_PATH" -executeMethod "$METHOD_NAME" -quit -batchmode -nographics | tee "$LOG_FILE"